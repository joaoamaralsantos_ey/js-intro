/**
 * logical OR
 */
exports.or = function(a, b) {

    if(typeof(a) === 'boolean'){

        if(a === false && b === false){
            return false;
        }
        else{
            return true;
        }
    }
    else{
        return a;
    }

};

/**
 * logical AND
 */
exports.and = function(a, b) {

    if(typeof(a) === 'boolean'){
        
        if(a === true && b === true){
            return true;
        }
        else{
            return false;
        }
    }
    else{
        return b;
    }

};
