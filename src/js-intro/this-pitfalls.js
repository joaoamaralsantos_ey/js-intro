// get a collection of all button elements on the page
var buttons = document.getElementsByTagName('button');

for (var i = 0; i < buttons.length; i++) {

  buttons[i].onclick = function() {

    // log the button pressed to the console
    console.log(this.innerText + ' was pressed!');

    // make sure every button on page is only pressed once
    disableButton().call(this);
  }
}

function disableButton() {
  this.disabled = true;
}