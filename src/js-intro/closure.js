var funcs = [];

for (var i = 0; i < 3; i++) {

  // push will create new execution context with reference to i
  funcs.push(makeClosure(i));

  //funcs.push(console.log.bind(null, i)) estou a criar 

}

funcs[0](); // 3
funcs[1](); // 
funcs[2](); // 3

// tenho sempre que invocar a função que quero que crie closure., so ai é criada o e
function makeClosure(i){
    return function(){
        console.log(i)
    }
}