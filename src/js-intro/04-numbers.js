/**
 * Convert a binary String to a Number
 */
exports.binaryToDecimal = function(str) {

    var result = 0;
    var size = str.length;  

    for(var i = 1; i < size; i++){
        
        result = result + Math.pow(2, size - i) * str[i-1];
    }

    return result;
};

/**
 * Add two Numbers with a precision of 2
 */
exports.add =  function(a, b) {

    var result = (a/10 + b/10) * 10;
    
    return result;
};

/**
 * Multiply two Numbers with a precision of 4
 */
exports.multiply =  function(a, b) { // usar 
    
    var result = (a*10000 * b*10000)/(10000 * 10000);
    
    return result;
};
