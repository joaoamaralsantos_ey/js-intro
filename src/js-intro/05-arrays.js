/**
 * Determine the location of an item in the array
 */
exports.indexOf = function(arr, item) {
    for(var i in arr){
        if(arr[i] === item){
            return parseInt(i); // é estupido
        }
    }
    return -1;
};

/**
 * Sum the items of an array
 */
exports.sum = function(arr) {
    var result = 0;
    for(var i in arr){

        result = result + arr[i];
    }
    return result;
};

/**
 * Remove all instances of an item from an array
 */
exports.remove = function(arr, item) {
   var new_a = [];

    for(var i in arr){
        if(arr[i] === item){
            continue;
        }
        else{
            new_a.push(arr[i]);
        }
    }

    return new_a;
};

/**
 * Remove all instances of an item from an array by mutating original array
 */
exports.removeWithoutCopy = function(arr, item) {

    while(arr.includes(item)){

        for(var i= 0; i < arr.length; i++){
            if(arr[i] === item){
                arr.splice(i,1)
                break;
            }
        }
    }

    return arr;
};

/**
 * Add an item to the end of the array
 */
exports.append = function(arr, item) {
    arr.push(item);
    return arr;
};

/**
 * Remove the last item of an array
 */
exports.truncate = function(arr) {
    arr.pop();
    return arr;
};

/**
 * Add an item to the beginning of an array
 */
exports.prepend = function(arr, item) {
    arr.unshift(item);
    return arr;
};

/**
 * Remove the first item of an array
 */
exports.curtail = function(arr) {
    arr.shift();
    return arr;
};

/**
 * Join two arrays together
 */
exports.concat = function(arr1, arr2) {
    return arr1.concat(arr2);
};

/**
 * Add an item to an array in the specified position
 */
exports.insert = function(arr, item, index) {
    arr.splice(index, 0, item);
    return arr;
};

/**
 * Count the number of occurrences of an item in an array
 */
exports.count = function(arr, item) {
    var count = 0;

    for(var i in arr){
        if(arr[i] === item){
            count = count + 1;
        }
    }

    return count;
};

/**
 * Find all items which container multiple occurrences in the array
 */
exports.duplicates = function(arr) {

    var duplicates_arr = [];

    for(var i = 0; i < arr.length; i++){
        for(var j = i +1; j < arr.length - 1; j++){
            if(arr[i] === arr[j]){
                duplicates_arr.push(arr[i]);
            }
        }
    }

    return duplicates_arr;
};

/**
 * Square each number in the array
 */
exports.square = function(arr) {
    return arr.map(x => x * x)
};

/**
 * Find all occurrenes of an item in an array
 */
exports.findAllOccurrences = function(arr, target) {

    var occurrences = [];

    for(var i in arr){
        if(arr[i] === target){
            occurrences.push(i);
        }
    }

    return occurrences;
};
