/**
 * Reduce duplicate characters to a desired minimum
 */
exports.reduceString = function(str, amount) {
    
    var arr = [];

    for(var i = 0; i < str.length - 1; i++){
        
        var s = '';

        while(i < str.length -1 && str[i] === str[i+1]){
            s = s + str[i];
            i++;
        }
        arr.push(s.concat(str[i]));
    }

    var arr_amount = arr.map( s => s.substr(0, amount))

    var reduce_string = '';

    for(var i in arr_amount){
        reduce_string = reduce_string + arr_amount[i];
        console.log(reduce_string)
    }

    return reduce_string;
};

/**
 * Wrap lines at a given number of columns without breaking words
 */
exports.wordWrap = function(str, cols) {

    var arr = str.split(' ');
    var wraped_string = '';
    var wraped_word = '';

    for(var i in arr){

        wraped_word = wraped_word + arr[i]

        console.log(typeof i)
        if(i == (arr.length - 1)){

            wraped_string = wraped_string + wraped_word; 
        }
        else if(wraped_word.length >= cols){

            wraped_string = wraped_string + wraped_word + '\n';
            wraped_word = '';
        }
        else{

            wraped_word = wraped_word + ' ';
        }
    }

    return wraped_string;
};

/**
 * Reverse a String
 */
exports.reverseString = function(str) {

    var new_str = '';

    for(var i = (str.length -1); i >= 0; i--){
        new_str = new_str + str[i];
    }

    return new_str;
};

/**
 * Check if String is a palindrome
 */
exports.palindrome = function(str){ 

    var size = str.length

    if(size === 1){
        return true;
    }
    else{

        for(var i = 0; i < size/2; i++){
            if(str[i] !== str[str.length - 1 - i]){
                return false;
            }
        }
        return true;
    }
};
