/**
 * Best Practices
 */

/**
 * Obtain user details
 */
exports.getUser = function() { // basta retornar logo o objecto, para não poluir o scope.
    var myObject = {
        name: 'Pedro',
        email: 'pedro.antoninho@academiadecodigo.org'
    };

    return myObject;
};

/**
 * Convert String to Number
 */
exports.parseNumber = function(num) {

    var x_position = num.indexOf('x');

   if(x_position !== -1 && x_position !== (num.length -1)){
    
       return parseInt(num.slice(0, x_position)) * parseInt(num.slice(x_position + 1, num.length))
   }
   else{
        return parseInt(num);
   }
};

/**
 * Tests for equality 
 */
exports.isEqual = function(val1, val2) {

    if(val1 === val2){
        return true;
    }
    else if(val1 !== val2){
        return false;
   }
    else{
        false;
    }
};
