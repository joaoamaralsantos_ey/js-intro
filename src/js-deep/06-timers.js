/**
 * Return a stop watch object with the following API:
 * getTime - return number of seconds elapsed
 * start - start counting time
 * stop - stop counting time
 * reset - sets seconds elapsed to zero
 */
exports.createStopWatch = function() {
    
    var counter = 0;
    var timer; 

    var incrementCounter = function(){
        counter = counter +1; 
    }

    return {
        getTime: function(){

            return counter;
        },

        start: function(){

            timer = setInterval(incrementCounter, 1000 );
        },
        stop: function(){
            clearInterval(timer)
        },

        reset: function(){

            counter = 0;

        }
    }
};
