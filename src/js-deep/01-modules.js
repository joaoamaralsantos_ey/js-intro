/**
 * Creates a counter module with an initial value, zero if not provided
 */
exports.createCounter = function (counter) {

    counter = counter || 0;

    return {
        get: function () {
            return counter;
        },

        increment: function () { //apenas incrementar e não retornar também. Métodos para obter estado e metodos para manipular estado. 
            counter = counter + 1;
        },

        reset: function () {
            counter = 0;
        }
    };
};

/**
 * Creates a person module with name and age
 * An initial name value should be provided and
 * an exception thrown if not
 */
exports.createPerson = function (name) {

    if (name === undefined || name === '') {    // if(!name) falsy: string vazia, zero, undefined, null, NaN, false
        throw new Error('You have to choose a name!!')
    }

    var age = 0;
    var name = name;

    return {
        getName: function () {
            return name;
        },
        getAge: function () {
            return age;
        },
        setAge: function (newAge) {
            age = newAge;
        }
    };

};
