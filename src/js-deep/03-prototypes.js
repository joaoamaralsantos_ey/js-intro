/**
 * Create a developer person object with a code method
 * that delegates to the provided person object
 */
exports.createDelegate = function (person) {

    var developerPerson = Object.create(person);

    developerPerson.code = function () { };

    return developerPerson;
};

/**
 * Borrow and invoke the person say method on top of the company object
 */
exports.borrowSayMethod = function (person, company) {

    return person.say.call(company);
};

/**
 * Iterate over all of the provided object own properties,
 * returning an array of key: value strings
 */
exports.iterate = function (obj) {
    var keys = Object.keys(obj);
    var values = Object.values(obj);

    var result = keys.map(function (x, index) {
        return x + ': ' + values[index];
    })

    return result;
};

/**
 * Add a repeatify method to all JavaScript Strings
 * 'string'.repeatify(3) should equal 'stringstringstring'
 * NOTE: This exercise exists for a pedagogic purpose only,
 * do not think doing stuff like this is a good idea...
 *
 * String already contains a repeat method, let's NOT use that one.
 */
exports.extendString = function () {

    String.prototype.repeatify = String.prototype.repeatify || function (times) {

        string = "";

        for (var i = 0; i < times; i++) {
            string = string + this
        }

        return string;
    }
};
