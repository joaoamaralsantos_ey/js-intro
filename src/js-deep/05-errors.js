/**
 * Return the result of invoking the provided function
 * If an exception is thrown, return the enclosed error message
 */
exports.callIt = function (fn) {
    try {

        return fn();

    } catch (err) {

        return err.message

    } finally {

        console.log('Error handled')
    }
};

/**
 * Return true if the provided arguments are equal,
 * throw an error with an enclosed message otherwise
 */
exports.assertEqual = function (a, b) {

/*    if (a === b) {  //colocar as excepções à cabeça, fica mais SUNSHINE PATH 
        return true;
    }
    else {
        throw new Error('a and b are not equal')
    }
*/
    if( a !== b){
        throw new Error('a and b are not equal')
    }

    return true;
};

/**
 * Return a custom error constructor with a timestamp property
 * indicating when the error occurred
 */
exports.createCustomError = function () {

    function CustomError(message) {

        // Error is not your typical constructor function
        var error = Error(message); // chain CustomError and Error constructors
        this.name = 'CustomError'; // the standard error name property
        this.message = error.message; // the standard message property
        this.stack = error.stack; // the stack trace
        this.timestamp = Date.now();
    };

    CustomError.prototype = Object.create(Error.prototype);
    CustomError.prototype.constructor = CustomError;

    return CustomError;
};
