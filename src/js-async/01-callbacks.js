/**
 * Invoke the callback and return the amount of time in miliseconds it took to execute
 */
exports.profileFunc = function(cb) {

    init_moment = Date.now();

    cb();

    return init_moment + Date.now();
};

/**
 * Invoke the async callback with the provided value after some delay
 */
exports.returnWithDelay = function(value, delay, cb) {

    var func = function() {
        cb(null, value);
    };

    setTimeout(func, delay);

};

/**
 * Invoke the async callback with an error after some delay
 */
exports.failWithDelay = function(delay, cb) {
    setTimeout(function() {
        cb(new Error, null); 
    }, delay);
};

/**
 * Return a promise that resolves after the specified delay
 * or rejects if the delay is negative or non-existent
 */
exports.promiseBasedDelay = function(delay) {

    if (!delay || delay < 0) {
        return Promise.reject(new Error());
    }
    return new Promise(function(resolve, reject) {

        return setTimeout(resolve, delay);
    });
};

/**
 * Use fetch to grab the contents of both urls and return
 * a promise resolving to the payload concatenation
 */
exports.urlToUpperCase = function(url1, url2) {
//Promiese.all
    return fetch(url1).then(function(data) {
        return fetch(url2).then(function(data2) {
            return data + data2;
        });
    });
};
